const sqlite3 = require('sqlite3');
const fs = require('fs');
const chalk = require('chalk');
let data;
module.exports.init = (fresh = false) => {
  if (fresh) {
    if (fs.existsSync('./database/data.sqlite')) {
      fs.unlinkSync('./database/data.sqlite');
    }
  }
  data = new sqlite3.Database('./database/data.sqlite');
  if (fresh) {
    const queries = fs.readFileSync('./database/schema.sql').toString();
    data.exec(queries);
  }
  console.log('database initialized.');
}
module.exports.putContact = (public_key, nickname) => {
  this.getContact(public_key, () => { // found contact so updating it
    const statement = data.prepare('UPDATE contacts SET nickname = ? WHERE public_key = ?');
    statement.run(nickname, public_key);
  }, () => { // not found contact so creating it
    const statement = data.prepare('INSERT INTO contacts (nickname, public_key) VALUES (?, ?)');
    statement.run(nickname, public_key);
  });
}
module.exports.updateContactLastTimeConnected = public_key => {
  const statement = data.prepare('UPDATE contacts SET last_time_connected = ? WHERE public_key = ?');
  statement.run(Date.now(), public_key);
}
module.exports.deleteContact = (public_key) => {
  data.run('DELETE FROM contacts WHERE public_key = ?', [public_key], err => {
    if (err)
      console.log(`${chalk.red('Err')}: error in deleting contact. (pk = ${public_key})`);
  });
}
module.exports.getContact = (public_key, found, not_found) => {
  data.all('SELECT * FROM contacts WHERE public_key = ?', [public_key], (err, contacts) => {
    if (err)
      return not_found();
    if (contacts.length > 0) {
      return found(contacts[0]);
    } else {
      return not_found();
    }
  });
}