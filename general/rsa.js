const NodeRSA = require('node-rsa');
module.exports.encrypt = (key, content) => {
  const node = new NodeRSA(`-----BEGIN PUBLIC KEY-----${key}-----END PUBLIC KEY-----`);
  return node.encrypt(content);
}
module.exports.decrypt = (key, content) => {
  const node = new NodeRSA(`-----BEGIN PRIVATE KEY-----${key}-----END PRIVATE KEY-----`);
  return node.decrypt(content);
}