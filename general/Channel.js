const NodeRSA = require('node-rsa');
module.exports = class Channel {
  constructor(node, socket, DB, removeFromList) {
    this.node = node;
    this.db = DB;
    this.socket = socket;
    this.event_listeners = {};
    this.once_event_listeners = {};
    this.src_private_key = new NodeRSA(`-----BEGIN PRIVATE KEY-----${node.private_key}-----END PRIVATE KEY-----`);
    this.src_private_key_str = node.private_key;
    this.last_time_connected = 0;
    this.message_queue = []; // {event, content}
    this.is_waiting_for_ack = false;

    this.socket.onmessage = data => {
      const [event, cipher] = data.data.split(',');
      switch (event) {
        case 'msg':
          const answer = JSON.parse(this.src_private_key.decrypt(cipher, 'utf8').toString());
          const sign = answer.s;
          if (this.dst_public_key) {
            if (!this.dst_public_key.verify({ e: answer.e, c: answer.c }, sign, 'base64', 'base64')) {
              console.log(`Err: invalid message on ${answer.e}`);
              this.sendDirect('err', { c: 100, m: 'invalid signiture' });
              return;
            }
          }
          const event = answer.e;
          const content = answer.c

          if (event === 'err') {
            console.log(`Error: ${content.c}: ${content.m}`);
            return;
          }

          if (this.event_listeners[event]) {
            this.event_listeners[event].forEach(callback => {
              callback(content);
            });
          }
          if (this.once_event_listeners[event]) {
            this.once_event_listeners[event].forEach(callback => {
              callback(content);
              delete this.once_event_listeners[event];
            });
          }
          this.sendAck();
          break;
        case 'error':
          console.log(error);
          break;
        case 'disconnect':
          console.log(e);
          console.log(`channel ${this.nickname} removed - disconnect`);
          removeFromList(this);
          break;
        case 'ack':
          console.log(`got ack: ${this.dst_public_key_str.substr(this.dst_public_key_str.length - 16)}`);
          if (this.message_queue.length === 0) {
            this.is_waiting_for_ack = false;
          } else {
            this.sendMessage();
          }
          break;
        default:
          console.log('unhandled data');
      }
    }
    this.sendMessage = () => {
      const message = this.message_queue.shift();
      console.log(`${this.shortKey()}: got ack`);
      if (message) {
        const { event, content } = message;
        const cipher = this.dst_public_key.encrypt(JSON.stringify({
          e: event,
          c: content,
          s: this.src_private_key.sign({ e: event, c: content }, 'base64', 'base64'),
        }), 'base64');
        this.socket.send(JSON.stringify(['msg', cipher]));
        this.is_waiting_for_ack = true;
      } else {
        this.is_waiting_for_ack = false;
      }
    };
  }
  sendAck() {
    this.socket.send(JSON.stringify(['ack']));
  }
  sendDirect(event, content) {
    this.message_queue.push({ event, content });
    console.log(`${this.shortKey()}: send direct!`);
    if (!this.is_waiting_for_ack) {
      this.sendMessage();
    }
  }
  broadCast(event, content) {
    const cipher = this.dst_public_key.encrypt(JSON.stringify({
      e: event,
      c: content,
      s: this.src_private_key.sign({ e: event, c: content }, 'base64', 'base64'),
    }), 'base64');
    this.socket.broadcast.send('msg', cipher);
  }
  on(event, callback) {
    if (!this.event_listeners[event]) {
      this.event_listeners[event] = [];
    }
    this.event_listeners[event].push(callback);
  }
  once(event, callback) {
    if (!this.event_listeners[event]) {
      this.once_event_listeners[event] = [];
    }
    this.once_event_listeners[event].push(callback);
  }
  setPublicKey(dst_public_key) {
    this.dst_public_key = new NodeRSA(`-----BEGIN PUBLIC KEY-----${dst_public_key}-----END PUBLIC KEY-----`);
    this.dst_public_key_str = dst_public_key;
  }
  setNickname(nickname) {
    this.nickname = nickname;
  }
  saveContact() {
    this.updateLastTimeConnected();
    this.db.putContact(this.dst_public_key_str, this.nickname);
  }
  updateLastTimeConnected() {
    this.last_time_connected = Date.now();
    this.db.updateContactLastTimeConnected(this.dst_public_key_str);
  }
  startHeartBeat(destroy) {
    // const sendHeartBeat = () => {
    //   if (!this.isAlive()) {
    //     destroy();
    //   } else {
    //     this.sendDirect('heartbeat', { t: Date.now() });
    //     setTimeout(sendHeartBeat, this.node.max_timeout);
    //   }
    // };
    // this.on('heartbeat', () => {
    //   this.updateLastTimeConnected();
    // });
    // sendHeartBeat();
  }
  isAlive() {
    return this.last_time_connected > Date.now() - 12000
  }
  shortKey() {
    this.dst_public_key_str.substr(this.dst_public_key_str.length - 16);
  }
}