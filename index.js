const io = require('socket.io')();
const config = require('./config.json');
const WebSocket = require('ws');
const Channel = require('./general/Channel');
const DB = require('./general/DB');
DB.init(false);

let channels = [];
const chunks = {};

const getChannelByPublicKey = public_key => {
  for (const i in channels) {
    if (channels[i].dst_public_key_str === public_key)
      return channels[i];
  }
  return null;
}
const notifyNewChunks = (channel) => {
  channels.forEach(ch => {
    if (ch.dst_public_key_str !== channel.dst_public_key_str) {
      ch.sendDirect('is_online', { public_key: channel.dst_public_key_str });
    }
  });
}

const port = 5000;
console.log(`started listening to port ${port}`);
const wss = new WebSocket.Server({
  port,
  perMessageDeflate: {
    zlibDeflateOptions: {
      // See zlib defaults.
      chunkSize: 1024,
      memLevel: 7,
      level: 3
    },
    zlibInflateOptions: {
      chunkSize: 10 * 1024
    },
    // Other options settable:
    clientNoContextTakeover: true, // Defaults to negotiated value.
    serverNoContextTakeover: true, // Defaults to negotiated value.
    serverMaxWindowBits: 10, // Defaults to negotiated value.
    // Below options specified as default values.
    concurrencyLimit: 10, // Limits zlib concurrency for perf.
    threshold: 1024 // Size (in bytes) below which messages
    // should not be compressed.
  }
});
wss.on('connection', socket => {
  const channel = new Channel(config, socket, DB, ch => {
    channels = channels.filter(item => ((ch.ip === item.ip && ch.port === item.port) ? null : ch));
    delete ch;
  });
  // step 0: get contact pubic key
  channel.on('connect_0', content => {
    channel.setPublicKey(content.pk);
    // step 1: send nickname
    channel.sendDirect('connect_1', { n: config.nickname });
    // step 2: get nickname
    channel.once('connect_2', content => {
      channel.setNickname(content.n);
      channel.saveContact();
      channel.sendDirect('connect_3', { 'm': 'you have been added to my memory!!' });
      channels.push(channel);
      notifyNewChunks(channel);
      channel.startHeartBeat(() => { // destroy the channel
        channels = channels.filter(ch => ((ch.socket.id === channel.socket.id) ? null : ch));
        console.log(`channel ${channel.nickname} removed`);
        delete channel;
      });
      console.log('secured connection.');
    });
  });

  // fetch node
  channel.on('fetch_node', content => {
    const node_public_key = content.pk;
    DB.getContact(node_public_key, contact => {
      channel.sendDirect('node_found', contact);
      console.log(`${channel.shortKey()}: found contact`);
    }, () => {
      channel.sendDirect('node_not_found', {})
      console.log(`${channel.shortKey()}: contact not found`);
    });
  });
  // follow node
  channel.on('follow_node', content => {
    const contact_public_key = content.pk;
    DB.getContact(contact_public_key, contact => {
      channel.sendDirect('node_followed', { pk: contact.public_key, n: contact.nickname });
    }, () => {
      channel.sendDirect('node_not_found', {});
    });
  });
  // recieve indirect message
  channel.on('indirect_message', content => {
    const { id, index, chunk, dst_public_key, count, store } = content;
    console.log(`${channel.shortKey()}: recieved message (${id}-${index}).`);
    const dst_channel = getChannelByPublicKey(dst_public_key);
    // check if destination node is connected
    if (dst_channel) {
      console.log(`${dst_channel.shortKey()}: dst -> alive!`);
      dst_channel.sendDirect('chunk', { id, index, chunk, count });
    }
    if (store) // if sender wants to store to chunk in network
      channels.forEach(data_channel => {
        if (data_channel.dst_public_key_str === (dst_channel || {}).dst_public_key_str)
          return false;
        if (data_channel.dst_public_key_str === channel.dst_public_key_str)
          return false;
        data_channel.sendDirect('data_chunk', { id, index, chunk, count, dst_public_key });
      });
  });
});
