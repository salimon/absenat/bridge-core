-- create contacts table
CREATE TABLE if NOT EXISTS contacts (
  public_key TEXT PRIMARY KEY,
  nickname TEXT,
  last_time_connected INTEGER
);
-- create connections table
CREATE TABLE IF NOT EXISTS connections (
  public_key_src TEXT,
  public_key_dst TEXT
);
CREATE INDEX pka ON connections (public_key_src);
CREATE INDEX pkb ON connections (public_key_dst);